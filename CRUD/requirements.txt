click==8.0.4
importlib-metadata==4.11.2
tabulate==0.8.9
typing_extensions==4.1.1
zipp==3.7.0
