## Activar el virtualizador de python

```sh
virtual-py venv
```

## Desconectar el virtualizador

```sh
disconnect
```

## Exportar nuestro archivo

```sh
  export FLASK_APP=main.py
  # Correr nuestro servidor
  flask run main.py
  flask run --host=0.0.0.0
```

## Para activar el debug mode escribir lo siguiente en la consola:

```sh
  export FLASK_DEBUG=1
  echo $FLASK_DEBUG
```
